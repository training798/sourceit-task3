package com.task3;
import java.io.*;
import java.util.*;
public class Task33 {
	public static void main(String[] args)
    {
		int n = 4;		  
        String words[]
            = { "apple", "happy", "love", "star" };
        Arrays.sort(words);
        System.out.println(
            "The words in alphabetical order are: ");
        for (int i = 0; i < n; i++) {
            System.out.println(words[i]);
        }
    }
}

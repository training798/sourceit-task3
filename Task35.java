package com.task3;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Task35 {
	public static List<String> breakSentenceSmart(String text, int maxWidth) {

	    StringTokenizer stringTokenizer = new StringTokenizer(text, " ");
	    List<String> lines = new ArrayList<String>();
	    StringBuilder currLine = new StringBuilder();
	    while (stringTokenizer.hasMoreTokens()) {
	        String word = stringTokenizer.nextToken();

	        boolean wordPut=false;
	        while (!wordPut) {
	            if(currLine.length()+word.length()==maxWidth) {
	                currLine.append(word);
	                wordPut=true;
	            }
	            else if(currLine.length()+word.length()<=maxWidth) {
	                if(stringTokenizer.hasMoreTokens()) {
	                    currLine.append(word + " ");
	                }else{
	                    currLine.append(word);
	                }
	                wordPut=true;
	            }else{
	                if(word.length()>maxWidth) {
	                    int lineLengthLeft = maxWidth - currLine.length();
	                    String firstWordPart = word.substring(0, lineLengthLeft);
	                    currLine.append(firstWordPart);
	                    word = word.substring(lineLengthLeft);
	                }
	                lines.add(currLine.toString());
	                currLine = new StringBuilder();
	            }
	        }
	    }
	    if(currLine.length()>0) {
	        lines.add(currLine.toString());
	    }
	    return lines;
	}
}
package com.task3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class Task31 {
	private String[] findRing(String... words) {
	    for (int i = 0; i < words.length; i++) {
	        String word = words[i];
	        String[] result = findRingForHeadAndTail(word, dropElementWithIndex(i, words));
	        if ((result.length == words.length - 1) &&
	                (word.charAt(0) == result[result.length - 1].charAt(result[result.length - 1].length() - 1))) {
	            return concatHeadAndTail(word, result);
	        }
	    }
	    return new String[]{"Немає рішення"};
	}

	private String[] findRingForHeadAndTail(
	        String head, 
	        String... tail 
	) {
	    if (tail.length == 0) {
	        return new String[0];
	    }

	    if (tail.length == 1) {
	        if (tail[0].charAt(0) == head.charAt(head.length() - 1)) {
	            return tail;
	        } else {
	            return new String[0];
	        }
	    }

	    for (int i = 0; i < tail.length; i++) {
	        String word = tail[i];
	        if (word.charAt(0) == head.charAt(head.length() - 1)) { 
	            String[] result = findRingForHeadAndTail(
	                    word, 
	                    dropElementWithIndex(i, tail)
	            ); 
	            if (result.length == tail.length - 1) { 
	                return concatHeadAndTail(word, result); 
	            }
	        }
	    }
	    return new String[0];
	}

	private String[] concatHeadAndTail(String head, String... tail) {
	    return Stream.concat(Stream.of(head), Stream.of(tail)).collect(Collectors.toList()).toArray(new String[tail.length + 1]);
	}

	private String[] dropElementWithIndex(int i, String... words) {
	    List<String> result = new ArrayList<>();
	    for (int j = 0; j < words.length; j++) {
	        if (j != i) {
	            result.add(words[j]);
	        }
	    }
	    return result.toArray(new String[words.length - 1]);
	}
	@Test
	public void findRing() {
	    System.out.println(Arrays.toString(findRing("заноза", "алфавіт", "таз","табурет")));
	    System.out.println(Arrays.toString(findRing("squirrel", "eyes", "klondlike", "lasik")));
	    System.out.println(Arrays.toString(findRing("why", "count", "where", "dance")));
	    System.out.println(Arrays.toString(findRing("зірка", "сонце", "зірка")));
	    System.out.println(Arrays.toString(findRing("зима", "авокадо", "люстерко")));
	}

}

